package nl.igorski.mwengine.example;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Vector;
import java.util.concurrent.Phaser;

import nl.igorski.mwengine.MWEngine;
import nl.igorski.mwengine.core.ABiquadHPFilter;
import nl.igorski.mwengine.core.ABiquadLPFilter;
import nl.igorski.mwengine.core.AudioChannel;
import nl.igorski.mwengine.core.BaseAudioEvent;
import nl.igorski.mwengine.core.BufferUtility;
import nl.igorski.mwengine.core.Delay;
import nl.igorski.mwengine.core.Drivers;
import nl.igorski.mwengine.core.Filter;
import nl.igorski.mwengine.core.JavaUtilities;
import nl.igorski.mwengine.core.LPFHPFilter;
import nl.igorski.mwengine.core.Limiter;
import nl.igorski.mwengine.core.Notifications;
import nl.igorski.mwengine.core.PitchShifter;
import nl.igorski.mwengine.core.ProcessingChain;
import nl.igorski.mwengine.core.ResLpHpFilter;
import nl.igorski.mwengine.core.ReverbSM;
import nl.igorski.mwengine.core.SWIGTYPE_p_AudioBuffer;
import nl.igorski.mwengine.core.SampleEvent;
import nl.igorski.mwengine.core.SampleManager;
import nl.igorski.mwengine.core.SampleUtility;
import nl.igorski.mwengine.core.SampledInstrument;
import nl.igorski.mwengine.core.SequencerController;
import nl.igorski.mwengine.core.SynthEvent;
import nl.igorski.mwengine.core.SynthInstrument;

public final class MWEngineActivity extends AppCompatActivity {
    /**
     * IMPORTANT : when creating native layer objects through JNI it
     * is important to remember that when the Java references go out of scope
     * (and thus are finalized by the garbage collector), the SWIG interface
     * will invoke the native layer destructors. As such we hold strong
     * references to JNI Objects during the application lifetime
     */
    private Limiter             _limiter;
    private LPFHPFilter         _lpfhpf;
    private SynthInstrument     _synth1;
    private SynthInstrument     _synth2;
    private String              _curSampleName;
    private SampledInstrument   _sampler;
    private SampledInstrument   _sampler2;
    private SampleEvent         _sampleEvent;
    private SampleEvent         _sampleEvent2;
    private int                 _sampleLenght;
    private int                 _sampleLenght2;
    private PitchShifter        _pitchShifter;
    private PitchShifter        _pitchShifter2;
    private ABiquadHPFilter     _filter;
    private ABiquadLPFilter     _bqLpfilter;
    private ABiquadHPFilter     _bqHpfilter;
    private Limiter             _flimiter;
    private FilterHandler       filterHandler;
//    private Filter              _filter;
    private Filter              _filter2;
//    private ResLpHpFilter       _filter2;
    private Phaser              _phaser;
    private Delay               _delay;
    private int                 _delayMin;
    private int                 _delayMax;
    private ReverbSM            _reverbSm;
    private ReverbSM            _reverbSm2;
    private MWEngine            _engine;
    private SequencerController _sequencerController;
    private Vector<SynthEvent>  _synth1Events;
    private Vector<SynthEvent>  _synth2Events;
    private Vector<SampleEvent> _drumEvents;
    private SynthEvent          _liveEvent;

    private boolean _sequencerPlaying = false;
    private boolean _isRecording      = false;
    private boolean _inited           = false;
    private boolean _isPitchShifterOn = false;

    // AAudio is only supported from Android 8/Oreo onwards.
    private boolean _supportsAAudio     = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O;
    private Drivers.types  _audioDriver = _supportsAAudio ? Drivers.types.AAUDIO : Drivers.types.OPENSL;

    private int SAMPLE_RATE;
    private int BUFFER_SIZE;
    private int OUTPUT_CHANNELS = 2; // 1 = mono, 2 = stereo

    private float minFilterCutoff = 50.0f;
    private float maxFilterCutoff;
    private float minABiquadFilterCutoff = 20.0f;
    private float maxABiquadFilterCutoff = 10000.0f;
    private float curABiquadFilterCutoff;
    private float curABiquadFilterQ;

    private static int STEPS_PER_MEASURE = 16;  // amount of subdivisions within a single measure
    private static String LOG_TAG = "MWENGINE"; // logcat identifier
    private static int PERMISSIONS_CODE = 8081981;
    private static String DEBUG_TAG = "Main";

    /* public methods */

    /**
     * Called when the activity is created. This also fires
     * on screen orientation changes.
     */
    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        // these may not necessarily all be required for your use case (e.g. if you're not recording
        // from device audio inputs or reading/writing files) but are here for self-documentation

        if ( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ) {
            String[] PERMISSIONS = {
                    Manifest.permission.RECORD_AUDIO, // RECORD_AUDIO must be granted prior to engine.start()
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };
            // Check if we have all the necessary permissions, if not: prompt user
            int permission = checkSelfPermission( Manifest.permission.RECORD_AUDIO );
            if ( permission == PackageManager.PERMISSION_GRANTED )
                init();
            else
                requestPermissions( PERMISSIONS, PERMISSIONS_CODE );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if ( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ) {
            if (requestCode != PERMISSIONS_CODE) return;
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];
                if (permission.equals(Manifest.permission.RECORD_AUDIO) && grantResult == PackageManager.PERMISSION_GRANTED) {
                    init();
                } else {
                    requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_CODE);
                }
            }
        }
    }

    /**
     * Called when the activity is destroyed. This also fires
     * on screen orientation changes, hence the override as we need
     * to watch the engines memory allocation outside of the Java environment
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        flushSong();        // free memory allocated by song
        _engine.dispose();  // dispose the engine
        Log.d( LOG_TAG, "MWEngineActivity destroyed" );
    }

    private void init() {
        if ( _inited )
            return;

        Log.d( LOG_TAG, "initing MWEngineActivity" );

        // STEP 1 : preparing the native audio engine

        _engine = new MWEngine( getApplicationContext(), new StateObserver() );
        MWEngine.optimizePerformance( this );

        // get the recommended buffer size for this device (NOTE : lower buffer sizes may
        // provide lower latency, but make sure all buffer sizes are powers of two of
        // the recommended buffer size (overcomes glitching in buffer callbacks )
        // getting the correct sample rate upfront will omit having audio going past the system
        // resampler reducing overall latency

        BUFFER_SIZE = MWEngine.getRecommendedBufferSize( getApplicationContext() );
        SAMPLE_RATE = MWEngine.getRecommendedSampleRate( getApplicationContext() );
//        int maxBuffer = MWEngine.getMaximumBufferSize(getApplicationContext());
//        int minBuffer = MWEngine.getMinimumBufferSize(getApplicationContext());

        Log.d( LOG_TAG, "RecommendedBufferSize" + BUFFER_SIZE );
//        Log.d( LOG_TAG, "MaximumBufferSize" + maxBuffer );
//        Log.d( LOG_TAG, "MinimumBufferSize" + minBuffer );
        Log.d( LOG_TAG, "RecommendedSampleRate" + SAMPLE_RATE );

        _engine.createOutput( SAMPLE_RATE, BUFFER_SIZE, OUTPUT_CHANNELS, _audioDriver );

        // STEP 2 : let's create some music !

        _drumEvents   = new Vector<SampleEvent>();

        _engine.setup(SAMPLE_RATE, BUFFER_SIZE, OUTPUT_CHANNELS);

        setupSong();

        // STEP 3 : start your engine!
        // Starts engines render thread (NOTE: sequencer is still paused)
        // this ensures that audio will be output as appropriate (e.g. when
        // playing live events / starting sequencer and playing the song)

        _engine.start();

        // STEP 4 : attach event handlers to the UI elements (see main.xml layout)

        if ( !_supportsAAudio ) {
            findViewById( R.id.DriverSelection ).setVisibility( View.GONE );
        } else {
            ((Spinner) findViewById( R.id.DriverSpinner )).setOnItemSelectedListener( new DriverChangeHandler() );
        }

        ((Spinner) findViewById( R.id.SoundSpinner )).setOnItemSelectedListener( new SoundChangeHandler() );

        findViewById( R.id.PlayPauseButton ).setOnClickListener(v -> {
            _sequencerPlaying = !_sequencerPlaying;
            _engine.getSequencerController().setPlaying( _sequencerPlaying );
            ((Button) v ).setText( _sequencerPlaying ? R.string.pause_btn : R.string.play_btn );
            if(_sequencerPlaying) _sampler.addEvent(_sampleEvent, true);//_sampleEvent.play();
            else _sampler.removeEvent(_sampleEvent, true);//_sampleEvent.stop();
        });


        ((SeekBar)findViewById( R.id.loop_startSlider )).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int val = (int)(( progress / 100f ) * _sampleLenght);
                _sampleEvent.setBufferRangeStart( val );
                Log.d(DEBUG_TAG, "loop start: " + val + " sample lenght: " + _sampleLenght);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.loop_endSlider )).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int val = (int)(( progress / 100f ) * _sampleLenght);
                _sampleEvent.setBufferRangeEnd( val );
                Log.d(DEBUG_TAG, "loop end: " + val + " sample lenght: " + _sampleLenght);

            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.PitchSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampleEvent.setPlaybackRate( progress / 50f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.FilterCutoffSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                filterHandler.onCutoffChanged(progress / 100f);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.FilterQSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                filterHandler.onQChanged(progress / 100f);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.RoomSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _reverbSm.setRoomSize( progress / 100f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.WetSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _reverbSm.setWet( progress / 100f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.channel1volume )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampler.getAudioChannel().setVolume(progress / 100f);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });



        findViewById( R.id.PlayPauseButton2 ).setOnClickListener(v -> {
            _sequencerPlaying = !_sequencerPlaying;
            _engine.getSequencerController().setPlaying( _sequencerPlaying );
            (( Button ) v ).setText( _sequencerPlaying ? R.string.pause_btn : R.string.play_btn );
            if(_sequencerPlaying) _sampleEvent2.play();
            else _sampleEvent2.stop();
        });


        ((SeekBar)findViewById( R.id.loop_startSlider2 )).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampleEvent2.setBufferRangeStart( (int)(( progress / 100f ) * _sampleLenght2) );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.loop_endSlider2 )).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampleEvent2.setBufferRangeEnd( (int)(( progress / 100f ) * _sampleLenght2) );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.PitchSlider2 )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampleEvent2.setPlaybackRate( progress / 50f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.FilterCutoffSlider2 )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _filter2.setCutoff(( progress / 100f ) * ( maxFilterCutoff - minFilterCutoff ) + minFilterCutoff );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.FilterQSlider2 )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _filter2.setResonance((float) Math.sqrt(((100 - progress) / 50f )) / 2 ); //( float ) ( Math.sqrt( 1 ) / 2 )
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.RoomSlider2 )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _reverbSm2.setRoomSize( progress / 100f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.WetSlider2 )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _reverbSm2.setWet( progress / 100f );
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        (( SeekBar ) findViewById( R.id.channel2volume )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _sampler2.getAudioChannel().setVolume(progress / 100f);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });


        (( SeekBar ) findViewById( R.id.VolumeSlider )).setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _engine.setVolume(progress / 100f);
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        _inited = true;
    }

    /* protected methods */

    protected void setupSong() {
        _sequencerController = _engine.getSequencerController();
//        _sequencerController.setTempoNow( 130.0f, 4, 4 ); // 130 BPM in 4/4 time
//        _sequencerController.updateMeasures( 1, STEPS_PER_MEASURE ); // we'll loop just a single measure with given subdivisions

        // cache some of the engines properties

        final ProcessingChain masterBus = _engine.getMasterBusProcessors();


        // create a lowpass filter to catch all low rumbling and a limiter to prevent clipping of output :)

        _lpfhpf  = new LPFHPFilter(( float )  MWEngine.SAMPLE_RATE, 55, OUTPUT_CHANNELS );
//        _limiter = new Limiter( 0.3f, 0.5f, 0.6f );

        masterBus.addProcessor( _lpfhpf );
//        masterBus.addProcessor( _limiter );

        // STEP 2 : let's create some instruments =D
        String bird = "bird.wav";
        String bach = "bach_48.wav";
        String bach2 = "bach2_48.wav";
        String bach3 = "bach3_48.wav";

        // Load some samples from the packaged assets folder into the SampleManager
        loadWAVAsset( "bird.wav", "bird" );
//
        loadWAVAsset( "bach2_48.wav", "bach2" );
        loadWAVAsset("guitare.wav", "guitare");
        loadWAVAsset("bird.wav", "bird");
        loadWAVAsset("piano_grave.wav", "piano");
        loadWAVAsset("bonjour-hello48_16bit.wav", "bonjour");
        loadWAVAsset("sanza.wav", "sanza");




        _sampler = new SampledInstrument();


        filterHandler = new FilterHandler();

        // add a filter
        maxFilterCutoff = ( float ) SAMPLE_RATE / 8;
        _bqLpfilter = new ABiquadLPFilter(
                10000, .0f, //( float ) ( Math.sqrt( 1 ) / 2 ),
                20, 20000, OUTPUT_CHANNELS);
        _bqHpfilter = new ABiquadHPFilter(
                20, .0f, //( float ) ( Math.sqrt( 1 ) / 2 ),
                20, 20000, OUTPUT_CHANNELS
        );
        _flimiter = new Limiter( 0.3f, 0.5f, 0.9f ); // att = 10f
        _flimiter.setKnee(0.6f);
//        limiter internal values:
        //    att  =  0.025, rel  =  0.000243, thresh = ( hard ) 0.707946


        _reverbSm = new ReverbSM();
        _reverbSm.setWet(0.f);
        _reverbSm.setDry(0.5f);
        _reverbSm.setDamp(0.7f);
        _reverbSm.setWidth(1);

        _sampleEvent = new SampleEvent(_sampler);
        _sampleEvent.setSample(SampleManager.getSample("guitare"));
        _sampleLenght = SampleManager.getSampleLength("guitare");
        _sampleEvent.setBufferRangeStart(0);
        _sampleEvent.setBufferRangeEnd(_sampleLenght);
        _sampleEvent.setRangeBasedPlayback(true);
//        _sampleEvent.setDuration(_sampleLenght);
        _sampleEvent.setLoopeable(true, 0);
//        _sampler.addEvent(_sampleEvent, true);
        _sampler.getAudioChannel().getProcessingChain().addProcessor(_bqLpfilter);
        _sampler.getAudioChannel().getProcessingChain().addProcessor(_bqHpfilter);
        _sampler.getAudioChannel().getProcessingChain().addProcessor(_flimiter);
        _sampler.getAudioChannel().getProcessingChain().addProcessor(_reverbSm);
        _sampler.getAudioChannel().setVolume(0.7f);


        _sampler2 = new SampledInstrument();

        // add a filter
        _filter2 = new Filter(
                maxFilterCutoff / 2, 1.0f, //( float ) ( Math.sqrt( 1 ) / 2 ),
                minFilterCutoff, maxFilterCutoff, OUTPUT_CHANNELS
        );

        _reverbSm2 = new ReverbSM();
        _reverbSm2.setWet(0.f);
        _reverbSm2.setDry(0.5f);
        _reverbSm2.setDamp(0.7f);
        _reverbSm2.setWidth(1);

        _sampleEvent2 = new SampleEvent(_sampler2);
        _sampleEvent2.setSample(SampleManager.getSample("bonjour"));
//        _sampleEvent2.setSample(SampleManager.getSample(bach2));
        _sampleLenght2 = SampleManager.getSampleLength("bonjour");
//        _sampleLenght2 = _sampleEvent2.getOriginalEventLength();
        _sampleEvent2.setBufferRangeStart(0);
        _sampleEvent2.setBufferRangeEnd(_sampleLenght2);;
//        _sampleEvent2.setRangeBasedPlayback(true);
        _sampleEvent2.setLoopeable(true, 0);
//        _sampler2.addEvent(_sampleEvent2, true);
        _sampler2.getAudioChannel().getProcessingChain().addProcessor(_filter2);
        _sampler2.getAudioChannel().getProcessingChain().addProcessor(_reverbSm2);
        _sampler2.getAudioChannel().setVolume(0.7f);


    }

    protected void flushSong() {
        // this ensures that Song resources currently in use by the engine are released

        _engine.pause();

        // calling 'delete()' on a BaseAudioEvent invokes the
        // native layer destructor (and removes it from the sequencer)

        for ( final BaseAudioEvent event : _drumEvents )
            event.delete();
        _sampleEvent.delete();

        // clear Vectors so all references to the events are broken

        _drumEvents.clear();

        // detach all processors from engine's master bus

        _engine.getMasterBusProcessors().reset();

        // calling 'delete()' on all instruments invokes the native layer destructor
        // (and frees memory allocated to their resources, e.g. AudioChannels, Processors)

        _sampler.delete();

        // allow these to be garbage collected

        _sampler = null;

        // and these (garbage collection invokes native layer destructors, so we'll let
        // these processors be cleared lazily)

        _filter = null;
        _reverbSm = null;
        _lpfhpf = null;
        _flimiter = null;


        // flush sample memory allocated in the SampleManager
        SampleManager.flushSamples();
    }

    @Override
    public void onWindowFocusChanged( boolean hasFocus ) {
        Log.d( LOG_TAG, "window focus changed for MWEngineActivity, has focus > " + hasFocus );

        if ( !hasFocus ) {
            // suspending the app - halt audio rendering in MWEngine Thread to save CPU cycles
            if ( _engine != null )
                _engine.pause();
        }
        else {
            // returning to the app
            if ( !_inited )
                init();            // initialize this example application
            else
                _engine.unpause(); // resumes existing audio rendering thread
        }
    }

    /* event handlers */

    private class DriverChangeHandler implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
            String selectedValue = parent.getItemAtPosition(pos).toString();
            _audioDriver = selectedValue.toLowerCase().equals("aaudio") ? Drivers.types.AAUDIO : Drivers.types.OPENSL;
            _engine.setAudioDriver( _audioDriver );
        }
        @Override
        public void onNothingSelected(AdapterView<?> arg0) {}
    }

    private class SoundChangeHandler implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
            String selectedValue = parent.getItemAtPosition(pos).toString();
            if (selectedValue.toLowerCase().equals("guitare")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("guitare"));
                _sampleLenght = SampleManager.getSampleLength("guitare");
                Log.d(DEBUG_TAG, "sample lenght guitare: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);

            } else if (selectedValue.toLowerCase().equals("bach")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("bach2"));
                _sampleLenght = SampleManager.getSampleLength("bach2");
                Log.d(DEBUG_TAG, "sample lenght bach2: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);

            } else if (selectedValue.toLowerCase().equals("bonjour")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("bonjour"));
                _sampleLenght = SampleManager.getSampleLength("bonjour");
                Log.d(DEBUG_TAG, "sample lenght bonjour: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);
            }
            else if (selectedValue.toLowerCase().equals("piano")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("piano"));
                _sampleLenght = SampleManager.getSampleLength("piano");
                Log.d(DEBUG_TAG, "sample lenght piano: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);

            }
            else if (selectedValue.toLowerCase().equals("sanza")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("sanza"));
                _sampleLenght = SampleManager.getSampleLength("sanza");
                Log.d(DEBUG_TAG, "sample lenght sanza: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);

            }
            else if (selectedValue.toLowerCase().equals("oiseau")) {
                _sampleEvent.delete();
                _sampleEvent = new SampleEvent(_sampler);
                _sampleEvent.setSample(SampleManager.getSample("bird"));
                _sampleLenght = SampleManager.getSampleLength("bird");
                Log.d(DEBUG_TAG, "sample lenght oiseau: " + _sampleLenght);
                _sampleEvent.setBufferRangeStart(0);
                _sampleEvent.setBufferRangeEnd(_sampleLenght);
                _sampleEvent.setRangeBasedPlayback(true);
                _sampleEvent.setLoopeable(true, 0);

            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> arg0) {}
    }

    private class FilterHandler {
        public void onCutoffChanged(float cutoff) {
            curABiquadFilterCutoff = cutoff;
            recalcParameters();
        }
        public void onQChanged(float q) {
            curABiquadFilterQ = q;
            recalcParameters();
        }
        private void recalcParameters() {
            // res values: 0..1 * 25db
            _bqHpfilter.setResonance(curABiquadFilterQ * 25.0f);
            _bqLpfilter.setResonance(curABiquadFilterQ * 25.0f);

            // offset modulated by amount of q: q=0 -> full offset, q=1 -> no offset
            float offset = 0.5f * (float) Math.pow((1-curABiquadFilterQ), 2);

            // further modulation for lo-pass offset according to freq to "push" a bit more in the low end
            float lpoffset = (float) Math.min(curABiquadFilterCutoff+0.5, 1) * offset;
            // clamp lo-pass cutoff to 1 so it wont go beyond maxfreq and remap to freq range
            float lpcut = (float) (Math.pow(Math.min(curABiquadFilterCutoff+lpoffset, 1), 2) *
                    (maxABiquadFilterCutoff - minABiquadFilterCutoff)) + minABiquadFilterCutoff;
            _bqLpfilter.setCutoff(lpcut);

            // clamp hi-pass to 0 so it wont become negative and climb up and remap to freq range
            float hpcut = (float) (Math.pow(Math.max(curABiquadFilterCutoff-offset, 0.0f), 2) *
                    (maxABiquadFilterCutoff - minABiquadFilterCutoff) + minABiquadFilterCutoff);
            _bqHpfilter.setCutoff( hpcut);
        }
    }


    /* state change message listener */

    private class StateObserver implements MWEngine.IObserver {
        private final Notifications.ids[] _notificationEnums = Notifications.ids.values(); // cache the enumerations (from native layer) as int Array
        public void handleNotification( final int aNotificationId ) {
            switch ( _notificationEnums[ aNotificationId ]) {

                case ERROR_HARDWARE_UNAVAILABLE:
                    Log.d( LOG_TAG, "ERROR : received Open SL error callback from native layer" );
                    // re-initialize thread
                    if ( _engine.canRestartEngine() ) {
                        _engine.dispose();
                        _engine.createOutput( SAMPLE_RATE, BUFFER_SIZE, OUTPUT_CHANNELS, _audioDriver );
                        _engine.start();
                    }
                    else {
                        Log.d( LOG_TAG, "exceeded maximum amount of retries. Cannot continue using audio engine" );
                    }
                    break;

                case MARKER_POSITION_REACHED:
                    Log.d( LOG_TAG, "Marker position has been reached" );
                    break;

                case RECORDING_COMPLETED:
                    Log.d( LOG_TAG, "Recording has completed" );
                    break;
            }
        }

        public void handleNotification( final int aNotificationId, final int aNotificationValue ) {
            switch ( _notificationEnums[ aNotificationId ]) {
                case SEQUENCER_POSITION_UPDATED:

                    // for this notification id, the notification value describes the precise buffer offset of the
                    // engine when the notification fired (as a value in the range of 0 - BUFFER_SIZE). using this value
                    // we can calculate the amount of samples pending until the next step position is reached
                    // which in turn allows us to calculate the engine latency

                    int sequencerPosition = _sequencerController.getStepPosition();
                    int elapsedSamples    = _sequencerController.getBufferPosition();

                    Log.d( LOG_TAG, "seq. position: " + sequencerPosition + ", buffer offset: " + aNotificationValue +
                            ", elapsed samples: " + elapsedSamples );
                    break;


                case RECORDED_SNIPPET_READY:
                    runOnUiThread( new Runnable() {
                        public void run() {
                            // we run the saving on a different thread to prevent buffer under runs while rendering audio
                            _engine.saveRecordedSnippet( aNotificationValue ); // notification value == snippet buffer index
                        }
                    });
                    break;

                case RECORDED_SNIPPET_SAVED:
                    Log.d( LOG_TAG, "Recorded snippet " + aNotificationValue + " saved to storage" );
                    break;
            }
        }
    }

    /* private methods */

    /**
     * convenience method for creating a new SynthEvent (a "musical instruction") for a given
     * SynthInstrument, this defaults to a note of a 16th note duration in this context
     *
     * @param synth     {SynthInstrument} the instrument that is to play the note
     * @param frequency {double} frequency in Hz of the note to play
     * @param position  {int}    position the position of the note within the bar
     */
    private void createSynthEvent( SynthInstrument synth, double frequency, int position ) {
        // duration in measure subdivisions, essentially a 16th note for the current STEPS_PER_MEASURE (16)

        final int duration = 1;
        final SynthEvent event = new SynthEvent(( float ) frequency, position, duration, synth );

        event.calculateBuffers();

        if ( synth == _synth1 )
            _synth1Events.add( event );
        else
            _synth2Events.add( event );
    }

    /**
     * convenience method for creating a new SampleEvent
     *
     * @param sampleName {String} identifier (inside the SampleManager) of the sample to use
     * @param position {int} position within the composition to place the event at
     */
    private void createDrumEvent( String sampleName, int position ) {
        final SampleEvent drumEvent = new SampleEvent( _sampler );
        drumEvent.setSample( SampleManager.getSample( sampleName ));
        drumEvent.positionEvent( 0, 16, position );
        drumEvent.addToSequencer(); // samples have to be explicitly added for playback

        _drumEvents.add( drumEvent );
    }

    /**
     * convenience method to load WAV files packaged in the APK
     * and read their audio content into MWEngine's SampleManager
     *
     * @param assetName {String} assetName filename for the resource in the /assets folder
     * @param sampleName {String} identifier for the files WAV content inside the SampleManager
     */
    private void loadWAVAsset( String assetName, String sampleName ) {
        final Context ctx = getApplicationContext();
        JavaUtilities.createSampleFromAsset(
                sampleName, ctx.getAssets(), ctx.getCacheDir().getAbsolutePath(), assetName
        );
    }
}