//
// Created by Sylvain on 2021-04-21.
// form: https://www.musicdsp.org/en/latest/Filters/131-cascaded-resonant-lp-hp-filter.html
//
// Cascaded resonant lowpass/hipass combi-filter
// The original source for this filter is from Paul Kellet from
// the archive. This is a cascaded version in Delphi where the
// output of the lowpass is fed into the highpass filter.
// Cutoff frequencies are in the range of 0<=x<1 which maps to
// 0..nyquist frequency

// input variables are:
// cut_lp: cutoff frequency of the lowpass (0..1)
// cut_hp: cutoff frequency of the hipass (0..1)
// res_lp: resonance of the lowpass (0..1)
// res_hp: resonance of the hipass (0..1)



//var n1,n2,n3,n4:single; // filter delay, init these with 0!
//fb_lp,fb_hp:single; // storage for calculated feedback
//const p4=1.0e-24; // Pentium 4 denormal problem elimination
//
//function dofilter(inp,cut_lp,res_lp,cut_hp,res_hp:single):single;
//begin
//  fb_lp:=res_lp+res_lp/(1-cut_lp);
//  fb_hp:=res_hp+res_hp/(1-cut_hp);
//  n1:=n1+cut_lp*(inp-n1+fb_lp*(n1-n2))+p4;
//  n2:=n2+cut_lp*(n1-n2);
//  n3:=n3+cut_hp*(n2-n3+fb_hp*(n3-n4))+p4;
//  n4:=n4+cut_hp*(n3-n4);
//  result:=inp-n4;
//end;


#ifndef MWENGINE_RESLPHPFILTER_H
#define MWENGINE_RESLPHPFILTER_H


#include "baseprocessor.h"

namespace MWEngine {
    class ResLpHpFilter : public BaseProcessor
    {
    public:
        ResLpHpFilter( float aCutoffFrequency, float aResonance, float aMinFreq, float aMaxFreq, int numChannels );
        ResLpHpFilter();
        ~ResLpHpFilter();

        void setCutoff( float frequency );
        float getCutoff();
        void setResonance( float resonance );
        float getResonance();

        void process( AudioBuffer* sampleBuffer, bool isMonoSource );


    protected:
        float _cutoff;
        float _resonance;
        float _minFreq;
        float _maxFreq;

        // used internally

        const SAMPLE_TYPE p4 = 1.0e-24; // Pentium 4 denormal problem elimination
        float SAMPLE_RATE;
        int amountOfChannels;

        SAMPLE_TYPE n1;
        SAMPLE_TYPE n2;
        SAMPLE_TYPE n3;
        SAMPLE_TYPE n4;
        SAMPLE_TYPE fb_lp;
        SAMPLE_TYPE fb_hp;
        SAMPLE_TYPE cut_lp;
        SAMPLE_TYPE cut_hp;
        SAMPLE_TYPE output;

//        SAMPLE_TYPE* in1;
//        SAMPLE_TYPE* in2;
//        SAMPLE_TYPE* out1;
//        SAMPLE_TYPE* out2;

    private:
        void init( float cutoff );
        void calculateParameters();
    };
} // E.O namespace MWEngine


#endif //MWENGINE_RESLPHPFILTER_H
