//
// Created by Sylvain on 2021-04-21.
//

#include "reslphpfilter.h"
#include "../global.h"
#include <math.h>

namespace MWEngine {

/**
 * @param aCutoffFrequency {float} desired cutoff frequency in Hz range 20-20000hz
 * @param aResonance {float} resonance range -25/25db
 * @param aMinFreq {float} minimum cutoff frequency in Hz
 * @param aMaxFreq {float} maximum cutoff frequency in Hz
 * @param numChannels {int} amount of output channels
 */
    ResLpHpFilter::ResLpHpFilter( float aCutoffFrequency, float aResonance,
                                      float aMinFreq, float aMaxFreq, int numChannels )
    {
        _resonance       = aResonance;
        _minFreq         = aMinFreq;
        _maxFreq         = aMaxFreq;
        amountOfChannels = numChannels;

        init( aCutoffFrequency );
    }

    ResLpHpFilter::ResLpHpFilter()
    {
        // resonance range 0...1
        _resonance       = 0; //( float ) sqrt( 1 ) / 2;
        _minFreq         = 20.f;
        _maxFreq         = 20000; //AudioEngineProps::SAMPLE_RATE / 8;
        amountOfChannels = AudioEngineProps::OUTPUT_CHANNELS;

        init( 0.5 );
    }

    ResLpHpFilter::~ResLpHpFilter()
    {
//        delete[] in1;
//        delete[] in2;
//        delete[] out1;
//        delete[] out2;
//
//        in1 = in2 = out1 = out2 = nullptr;
    }

/* public methods */

    void ResLpHpFilter::process( AudioBuffer* sampleBuffer, bool isMonoSource )
    {
        int bufferSize               = sampleBuffer->bufferSize;


        if ( amountOfChannels < sampleBuffer->amountOfChannels )
            isMonoSource = true;

        for ( int i = 0, l = sampleBuffer->amountOfChannels; i < l; ++i )
        {
            SAMPLE_TYPE* channelBuffer = sampleBuffer->getBufferForChannel( i );

            calculateParameters();

            for ( int j = 0; j < bufferSize; ++j )
            {
                SAMPLE_TYPE input = channelBuffer[ j ];

                n1 = n1+cut_lp * (input-n1+fb_lp*(n1-n2))+p4;
                n2 = n2-cut_lp * (n1-n2);
                n3 = n3+cut_hp * (n2-n3+fb_hp*(n3-n4))+p4;
                n4 = n4-cut_hp * (n3-n4);
                output = input-n4;

                channelBuffer[ j ] = output;
            }

            // save CPU cycles when source is mono
            if ( isMonoSource )
            {
                sampleBuffer->applyMonoSource();
                break;
            }
        }
    }

    void ResLpHpFilter::setCutoff( float frequency )
    {
        float offset = 0.25f * (1-_resonance);
        cut_lp = std::max( 0.0f, std::min( std::pow(frequency+offset, 2.0f), 1.0f ));
        cut_hp = std::max( 0.0f, std::min( std::pow(frequency-offset, 2.0f), 1.0f ));
        _cutoff = frequency;

        calculateParameters();
    }

    float ResLpHpFilter::getCutoff()
    {
        return _cutoff;
    }

    void ResLpHpFilter::setResonance( float resonance )
    {
        _resonance = resonance;
        calculateParameters();
    }

    float ResLpHpFilter::getResonance()
    {
        return _resonance;
    }

/* private methods */

    void ResLpHpFilter::init( float cutoff )
    {
        SAMPLE_RATE = ( float ) AudioEngineProps::SAMPLE_RATE;

        _cutoff     = 0.5;
        n1 = 0.0;
        n2 = 0.0;
        n3 = 0.0;
        n4 = 0.0;


//        in1  = new SAMPLE_TYPE[ amountOfChannels ];
//        in2  = new SAMPLE_TYPE[ amountOfChannels ];
//        out1 = new SAMPLE_TYPE[ amountOfChannels ];
//        out2 = new SAMPLE_TYPE[ amountOfChannels ];
//
//        for ( int i = 0; i < amountOfChannels; ++i )
//        {
//            in1 [ i ] = 0.0;
//            in2 [ i ] = 0.0;
//            out1[ i ] = 0.0;
//            out2[ i ] = 0.0;
//        }

        // using this setter caches appropriate values
 //       setCutoff( cutoff );
    }

    void ResLpHpFilter::calculateParameters()
    {
//        double cutoff;
//        cutoff = 2 * _cutoff / SAMPLE_RATE;



        // simplify: both filter will get same resonance so res_lp and res_hp is the same
//  fb_lp:=res_lp+res_lp/(1-cut_lp);
//  fb_hp:=res_hp+res_hp/(1-cut_hp);

        fb_lp = _resonance + _resonance / (1-cut_lp);
        fb_hp = _resonance + _resonance / (1-cut_hp);

    }

} // E.O namespace MWEngine